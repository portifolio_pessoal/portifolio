---
title: "Autobiografia"
date: 2024-10-06T17:58:09-03:00
draft: false
---

        
Nascido em 07 de abril de 2004 na cidade de São Paulo, me mudei para Ourinhos, no interior do mesmo estado, aos 8 anos de idade.   
No interior estudei no Colégio Drummond até completar o ensino médio, durante este percurso desenvolvi um profundo interesse na área de exatas, motivo pelo qual busquei participar em diversas olimpíadas de conhecimento, entre elas: Olimpíada Brasileira de Matemática, Olimpíada Brasileira de Astronomia, Olimpiada Brasileira de Física e a Olimpiada Brasileira de Ciências. Durante as diversas edições obti premiações com destaque para Medalha de Prata na OBMEP, Bronze na OBF, além de medalhas de Ouro na OBA e ONC.  
Em busca de desenvolver ainda mais meus conhecimentos e habilidades nestes campos optei por cursar Engenharia Mecatrônica na Escola Politécnica como curso superior, em busco de um currículo abrangente e experiência prática em pesquisa e projetos.  
Neste site explicarei mais afundo sobre projetos já realizados, além de projetos profissionais futuros.