---
title: 'Intercâmbio'
date: 2024-10-07T08:06:33-03:00
draft: false
---
![polimi](../../images/logo-polimi.png "Politecnico di Milano")
Em busca de elevar o escopo do meu conhecimento escolhi por participar do processo seletivo para Duplo Diploma oferecido pela Escola Politécnica. Dentre as opções disponíveis optei pelo Politecnico di Milano, no curso de Engenharia da Computação. O curso é oferecido no Campus Leonardo da universidade, o qual é demonstrado abaixo.
![leonardo](../../images/polimi_leonardo.jpg "Campus Leonardo")
O Politecnico di Milano se destaca pelo grande incentivo a pesquisa, referência em toda Europa, possuindo laboratórios de ponta, como o Túnel de Vento e uma Sala Limpa.  
Ademais, podemos destacar a grande flexibilidade na grade horária que a universidade oferece, permitindo que o estudante guie sua carreira acadêmica pelo caminho de sua preferência.  
O estudo em outro país é uma oportunidade excelente para entrar em contato com diferentes formas de pensamento e transmitir conhecimento, além da criação de uma rede de contatos internacional. Estes fatores permitem o desenvolvimento de competências essenciais para o desenvolvimento de uma carreira profissional.
O processo de intercâmbio começará em agosto de 2025, estudarei no Politecnico durante 2 anos, os quais tenho certeza que tirarei aprendizados que servirão grandemente para meu amadurecimento pessoal e profissional. 