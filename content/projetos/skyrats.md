---
title: 'Skyrats'
date: 2024-10-08T21:10:59-03:00
draft: false
---
![skyrats](../../images/Skyrats.svg "Logo Skyrats")
A Skyrats é o grupo de extensão de desenvolvimento de drone autônomos da USP. Ingressei na equipe no início do meu segundo ano na graduação, na subárea da mecânica da equipe.  
Como membro desta subárea participei de projetos de desenvolvimento de drones, realizando atividades de design e dimensionamento de drones, além de técnicas de manufatura como manufatura aditiva,laminação com fibra de vidro e moldes de compressão.
![cbr](../../images/cbrRobot.png "Logo CBR")  
Em 2023 também participei da Competição Brasileira de Robótica (CBR), sediada em Salvador, juntamente com outros membros da equipe. A competição tratava-se de desafios indoor, onde o drone deferia ser capaz de identificar posições demarcas, realizar pousos de precisão e movimentar objetos de forma autônoma.
A equipe ainda desenvolve projetos para outras competições, como a IMAV, também para as categorias de drones outdoors.  
Há ainda diversos projetos que não possuem foco em competição, como drones de filmagem, hexacópteros (diferentemente dos quadicópteros usados usualmente), além de capacitação para pilotagem de drones.