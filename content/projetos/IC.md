---
title: 'IC'
date : 2024-10-07T08:05:21-03:00
draft: false
---

O projeto "Criação de Atlas Anatômico para EIT" foi proposto pelo professor Marcos de Sales Guerra Tsuzuki, durante meu segundo ano do curso.  
O objetivo do projeto é o desenvolvimento de um método de regularização para a reconstrução de imagens de tomografia que resulte em menor perda de resolução e perturbação das imagens, em relação as técnicas aplicadas atualmente.  
O atlas anatômico consiste em imagens que armazenam informações anatômicas estatísticas a respeito da probabilidade de cada tecido estar associado a um determinado voxel da imagem.   
Com isto é possível fornecer novas condições de contorno para o algoritmo de reconstrução, possibilitando melhores resultados.Um exemplo de imagem de tomografia utilizado pode ser visto na figura abaixo.  
![tomografia](../../images/exemplo_tomografia.png "Imagem de Tomografia")
Para o desenvolvimento do atlas foram utilizadas 1410 imagens de tomografia, que passaram por processos, sequencialmente: Redução, Segmentação e padronização; até poderem ser comparadas para a realização do levantamento estaístico que gerou as imagens probabilísticas.  
No projeto foram gerados atlas para: Traqueia e vias aéreas, pulmões, ossos, além de regiões vazias no interior e exterior da pele. O atlas gerado pode ser visualizado a seguir.

![atlas](../../images/atlas_final.png "Atlas Anatômico")
